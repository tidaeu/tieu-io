function intersectAni(el: Element, afterCommand: string){

    const customObserver = new IntersectionObserver(
        (entries, animatedScrollObserver) => {
            entries.forEach((entry) =>{
                if(entry.isIntersecting)
                {
                    entry.target.classList.add(afterCommand);
                }
            });
        }
    );

    customObserver.observe(el);
}
    
export default{
    bind(el, binding){

        const before = "before-enter-" + binding.value;
        const after = "after-enter-" + binding.value;

        el.classList.add(before);
    
        //Creates and executes the IntersectionObserver Object
        intersectAni(el, after);
    }
}